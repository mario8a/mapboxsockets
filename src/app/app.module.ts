import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { MapaComponent } from './components/mapa/mapa.component';

// import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { SocketIoModule } from 'ngx-socket-io';
import { environment } from '../environments/environment';

// const config: SocketIoConfig = { url: 'http://localhost:5000', options: {} };




@NgModule({
  declarations: [
    AppComponent,
    MapaComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    SocketIoModule.forRoot(environment.socketConfig),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
